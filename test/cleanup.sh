#!/bin/sh
#cleanup.sh is a shell script that cleans up the files that
#are created by regress.sh
rm -rf *.s *.dot *.java *.class *.png meggy/*.class output.log t1 t2
rm -rf WorkingTestCases/*.s WorkingTestCases/*.dot WorkingTestCases/*.class 
