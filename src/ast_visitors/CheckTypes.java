package ast_visitors;

/** 
 * CheckTypes
 * 
 * This AST visitor traverses a MiniJava Abstract Syntax Tree and checks
 * for a number of type errors.  If a type error is found a SymanticException
 * is thrown
 * 
 * CHANGES to make next year (2012)
 *  - make the error messages between *, +, and - consistent <= ??
 *
 * Bring down the symtab code so that it only does get and set Type
 *  for expressions
 */

import ast.node.*;
import ast.visitor.DepthFirstVisitor;
import java.util.*;

import symtable.*;
import exceptions.InternalException;
import exceptions.SemanticException;

public class CheckTypes extends DepthFirstVisitor
{

	private SymTable mCurrentST;
	private ClassSTE mCurrentClass;
	private int mClass_offset = 0;
	private int mMethod_offset = 1;
	private boolean debug = false;

	public CheckTypes(SymTable st) {
		if(st==null) {
			throw new InternalException("unexpected null argument");
		}
		mCurrentST = st;
	}

	//========================= Overriding the visitor interface

	public void defaultOut(Node node) {
		System.err.println("Node not implemented in CheckTypes, " + node.getClass());
	}

	public void outAndExp(AndExp node)
	{
		if(debug) System.out.println(mCurrentST.getExpType(node.getLExp()).toString() + " && Left Expression Type");
		if(this.mCurrentST.getExpType(node.getLExp()) != Type.BOOL) {
			throw new SemanticException(
					"Invalid left operand type for operator &&",
					node.getLExp().getLine(), 
					node.getLExp().getPos());
		}

		if(this.mCurrentST.getExpType(node.getRExp()) != Type.BOOL) {
			throw new SemanticException(
					"Invalid right operand type for operator &&",
					node.getRExp().getLine(), node.getRExp().getPos());
		}

		this.mCurrentST.setExpType(node, Type.BOOL);
	}
	public void outMulExp(MulExp node) {

		Type lexpType = this.mCurrentST.getExpType(node.getLExp());
		Type rexpType = this.mCurrentST.getExpType(node.getRExp());

		if(lexpType != Type.BYTE) {
			throw new SemanticException(	
					"Invalid left operand type for operator *",
					node.getLExp().getLine(), 
					node.getLExp().getPos());
		}

		if(rexpType != Type.BYTE) {
			throw new SemanticException(
					"Invalid right operand type for operator &&",
					node.getRExp().getLine(), 
					node.getRExp().getPos());
		}

		this.mCurrentST.setExpType(node, Type.INT);
	}


	public void outPlusExp(PlusExp node)
	{
		Type lexpType = this.mCurrentST.getExpType(node.getLExp());
		Type rexpType = this.mCurrentST.getExpType(node.getRExp());
		
		if (((lexpType != Type.INT) && (lexpType != Type.BYTE)) || ((rexpType != Type.INT) && (rexpType != Type.BYTE))) {
		   throw new SemanticException(
		   	"Operands to + operator must be the same type",
		   		node.getLExp().getLine(),
		   		node.getLExp().getPos());
		   } 

		if ( (lexpType == Type.INT) || (lexpType == Type.BYTE) ) { 
			this.mCurrentST.setExpType(node, Type.INT); 
		} else {
			throw new SemanticException(
					"Operands to + operator must be Int or Byte",
					node.getLExp().getLine(),
					node.getLExp().getPos());
		}
	}
	public void outNegExp(NegExp node)
	{
		Type expType = this.mCurrentST.getExpType(node.getExp());
		if ( (expType != Type.INT)  && (expType != Type.BYTE) ) {
			throw new SemanticException(
					"Operands to Unary Minus must be Int or Byte",
					node.getExp().getLine(),
					node.getExp().getPos());
		}

		this.mCurrentST.setExpType(node, Type.INT);

	}
	public void outMinusExp(MinusExp node)
	{
		Type lexpType = this.mCurrentST.getExpType(node.getLExp());
		Type rexpType = this.mCurrentST.getExpType(node.getRExp());
		if ((lexpType==Type.INT  || lexpType==Type.BYTE) &&
				(rexpType==Type.INT  || rexpType==Type.BYTE)
		   ){
			this.mCurrentST.setExpType(node, Type.INT);
		} else {
			throw new SemanticException(
					"Operands to - operator must be Int or Byte",
					node.getLExp().getLine(),
					node.getLExp().getPos());
		}

	}	
	public void outEqualExp(EqualExp node)
	{
		Type lexpType = this.mCurrentST.getExpType(node.getLExp());
		Type rexpType = this.mCurrentST.getExpType(node.getRExp());

		if(lexpType == Type.BUTTON) {
			throw new SemanticException(
					"Operands to == operator must be Int or Byte",
					node.getRExp().getLine(),
					node.getRExp().getPos());
		}

		if ( (lexpType != rexpType ) && (
					( (lexpType != Type.INT)  && (lexpType != Type.BYTE) ) ||
					( (rexpType != Type.INT)  && (rexpType != Type.BYTE) ) ) ) {
			throw new SemanticException(
					"Mixed Operands to == operator must be Int or Byte",
					node.getLExp().getLine(),
					node.getLExp().getPos());
					}

		this.mCurrentST.setExpType(node, Type.BOOL);
	}	
	public void outIntegerExp(IntLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.INT);
	}

	public void outByteCast(ByteCast node)
	{
		Type byteType = this.mCurrentST.getExpType(node.getExp());
		if (( byteType == null ) || ((byteType != Type.INT) && (byteType != Type.BYTE)) ) {
			throw new SemanticException("Can only cast byte or int to a byte", node.getLine(), node.getPos());
		}
		this.mCurrentST.setExpType(node, Type.BYTE);
	}

	public void outColorExp(ColorLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.COLOR);
	}

	public void outMeggySetPixel(MeggySetPixel node)
	{
		Type xexpType = this.mCurrentST.getExpType(node.getXExp());
		Type yexpType = this.mCurrentST.getExpType(node.getYExp());
		Type colorType = this.mCurrentST.getExpType(node.getColor());
		if( xexpType==Type.BYTE && 
				yexpType==Type.BYTE && 
				colorType==Type.COLOR ) {
		} else {
			throw new SemanticException(
					"Invalid argument exception for MeggySetPixel",
					node.getLine(),
					node.getPos()
					);
		}
	}

	public void outBlockStatement(BlockStatement node)
	{
	}

	public void outMainClass(MainClass node)
	{
	}

	public void outProgram(Program node)
	{
	}

	public void outTrueExp(TrueLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.BOOL);
	}

	public void outFalseExp(FalseLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.BOOL);
	}

	public void outButtonExp(ButtonLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.BUTTON);
	}


	public void outMeggyCheckButton(MeggyCheckButton node)
	{
		if(this.mCurrentST.getExpType(node.getExp()) != Type.BUTTON)
			throw new SemanticException("Invalid argument for Meggy.CheckButton()", node.getLine(), node.getPos());

		this.mCurrentST.setExpType(node, Type.BOOL);
	}

	public void outIfStatement(IfStatement node)
	{
		if (this.mCurrentST.getExpType(node.getExp()) != Type.BOOL) {
			throw new SemanticException("Invalid condition type in if statement", node.getLine(), node.getPos());
		}
	}

	public void outWhileStatement(WhileStatement node)
	{
		if (this.mCurrentST.getExpType(node.getExp()) != Type.BOOL) {
			throw new SemanticException("Invalid condition type in if statement", node.getLine(), node.getPos());
		}
	}

	public void outMeggyDelay(MeggyDelay node)
	{
		if (this.mCurrentST.getExpType(node.getExp()) != Type.INT) {
			throw new SemanticException("Invalid condition type in if statement", node.getLine(), node.getPos());
		}
	}

	public void outMeggyToneStart(MeggyToneStart node)
	{
		Type toneType = this.mCurrentST.getExpType(node.getToneExp());
		Type durationType = this.mCurrentST.getExpType(node.getDurationExp());
		if( toneType==Type.TONE &&
				durationType==Type.INT ) {
		} else {
			throw new SemanticException(
					"Invalid argument exception for MeggyGetPixel",
					node.getLine(),
					node.getPos()
					);
		}
	}

	public void outToneExp(ToneLiteral node)
	{
		this.mCurrentST.setExpType(node, Type.TONE);
	}

	public void outMeggyGetPixel(MeggyGetPixel node)
	{
		Type xexpType = this.mCurrentST.getExpType(node.getXExp());
		Type yexpType = this.mCurrentST.getExpType(node.getYExp());
		if( xexpType==Type.BYTE && 
				yexpType==Type.BYTE ) {
			this.mCurrentST.setExpType(node, Type.COLOR);
		} else {
			throw new SemanticException(
					"Invalid argument exception for MeggyGetPixel",
					node.getLine(),
					node.getPos()
					);
		}
	}

	public void outNewExp(NewExp node) {
		STE ste = this.mCurrentST.lookup(node.getId());
		this.mCurrentST.setNodeSTE(node, ste);
		if (ste == null) {
			throw new SemanticException("New Expression Class is undefined", node.getLine(), node.getPos());
		}
		this.mCurrentST.setExpType(node, Type.getClassType(node.getId()));
	}

	public void outCallExp(CallExp node) {
		Type expType = typeCheck(node.getExp(), node.getId(), node.getArgs());
		this.mCurrentST.setExpType(node, expType);
	}

	public void outCallStatement(CallStatement node) {
		typeCheck(node.getExp(), node.getId(), node.getArgs());
	}

	public void outColorType(ColorType node)
	{

	}

	public void outVarDecl(VarDecl node)
	{
		VarSTE ste = (VarSTE) this.mCurrentST.lookup(node.getName());
		
		this.mCurrentST.setNodeSTE(node, ste);
		if (ste.isMember()) {
			ste.setLocation("Z", this.mClass_offset);

			this.mClass_offset += ste.getType().getAVRTypeSize();
		}
		else {
			ste.setLocation("Y", this.mMethod_offset);

			this.mMethod_offset += ste.getType().getAVRTypeSize();
		}
		
		if(ste.getType() == Type.VOID) {
			throw new SemanticException("Cannot declare a variable as type void", node.getLine(), node.getPos() );
		}
		if(ste.getType().isClass()) {
			ClassSTE classEntry = (ClassSTE) this.mCurrentST.lookup(ste.getType().getName());
			if(classEntry == null) {
				throw new SemanticException("Class " + node.getName() + " does not exist", node.getLine(), node.getPos());
			}
		}
	}
	public void outColorArrayType(ColorArrayType node)
	{

	}

	public void outVoidType(VoidType node)
	{

	}

	public void outByteType(ByteType node)
	{

	}

	public void outIdLiteral(IdLiteral node)
	{
		STE ste = this.mCurrentST.lookup(node.getLexeme());	 
		this.mCurrentST.setNodeSTE(node, ste);
		if (ste == null) {
			throw new SemanticException(
					"Undeclared variable " + node.getLexeme(),
					node.getLine(),
					node.getPos());
		}
		if (ste instanceof VarSTE) {
			VarSTE varEntry = (VarSTE) ste;
			this.mCurrentST.setExpType(node, varEntry.getType());
		}
	}

	public void outAssignStatement(AssignStatement node) {
		STE ste = this.mCurrentST.lookup(node.getId());
		this.mCurrentST.setNodeSTE(node, ste);

		if ( (ste == null) || (!(ste instanceof VarSTE)) ) {
			throw new SemanticException("Undeclared variable " + node.getId(), node.getLine(), node.getPos());
		}

		VarSTE varEntry = (VarSTE) ste;

		if( varEntry.getType() != this.mCurrentST.getExpType(node.getExp()) )
			throw new SemanticException(
					"Invalid expression type assigned to variable " 
					+ node.getId(), 
					node.getLine(), 
					node.getPos());
	}

	public void inMethodDecl(MethodDecl node)	{
		this.mCurrentST.pushScope(node.getName());

		String str = node.getName();
		VarSTE varEntry = (VarSTE)this.mCurrentST.lookup("this");
		varEntry.setLocation("Y", this.mMethod_offset);
		this.mMethod_offset += varEntry.getType().getAVRTypeSize();
	}

	public void outMethodDecl(MethodDecl node)	{
		this.mCurrentST.popScope();

		MethodSTE ste = (MethodSTE) mCurrentST.lookup(node.getName());
		

		ste.setVarNumBytes(this.mMethod_offset - 1);

		this.mMethod_offset = 1;
		mCurrentST.setNodeSTE(node, ste);
		

		Type retType = ste.getSignature().getReturnType();
		Type expType = mCurrentST.getExpType( node.getExp() ); 

		if ((retType != Type.INT) || (expType != Type.BYTE)) {
			if( (retType != expType) && (expType != null) )
				throw new SemanticException(
						"Invalid type returned from method "
						+ node.getName(), 
						node.getLine(), 
						node.getPos());
		}

	}

	public void inThisExp(ThisLiteral node)
	{
	}

	public void outThisExp(ThisLiteral node)
	{
		if( mCurrentClass == null )
			throw new InternalException("outThisExp: mCurrentClass==null");
		VarSTE ste = (VarSTE) this.mCurrentST.lookup("this"); 
		this.mCurrentST.setNodeSTE(node, ste);
		this.mCurrentST.setExpType(node, Type.getClassType(this.mCurrentClass.getName()));
	}

	public void visitThisLiteral(ThisLiteral node)
	{
		inThisExp(node);
		outThisExp(node);
	}
	public void outClassType(ClassType node) {

	}

	public void inTopClassDecl(TopClassDecl node) {
		this.mCurrentClass = ((ClassSTE)this.mCurrentST.lookup(node.getName()));
		this.mCurrentST.setNodeSTE(node, mCurrentClass);
		this.mCurrentST.pushScope(node.getName());
	}

	public void outTopClassDecl(TopClassDecl node) {
		this.mCurrentST.popScope();

		Type classType = Type.getClassType(node.getName());
		classType.setClassSize(this.mClass_offset);

		this.mClass_offset = 0; 
	}

	public void outFormal(Formal node) { 
		VarSTE varEntry = (VarSTE)this.mCurrentST.lookup(node.getName());

		varEntry.setLocation("Y", this.mMethod_offset);


		this.mMethod_offset += varEntry.getType().getAVRTypeSize(); 
	}

	public void outBoolType(BoolType node) {

	}

	public void outNotExp(NotExp node) {
		Type expType =  this.mCurrentST.getExpType(node.getExp());

		if (expType != Type.BOOL) {
			throw new SemanticException(
					"Invalid operand for ! operator",
					node.getLine(),
					node.getPos());
		}
		this.mCurrentST.setExpType(node, Type.BOOL);
	}

	public void outIntType(IntType node) {

	}

	public void outToneType(ToneType node) {

	}

	public void outLtExp(LtExp node)	{
		Type lexpType = this.mCurrentST.getExpType(node.getLExp());
		Type rexpType = this.mCurrentST.getExpType(node.getRExp());
		if (((lexpType != Type.INT) && (lexpType != Type.BYTE)) || ((rexpType != Type.INT) && (rexpType != Type.BYTE))) {
			throw new SemanticException(
					"Operands to < operator must be the same type",
					node.getRExp().getLine(),
					node.getRExp().getPos());
		}
		if ( (lexpType != Type.INT ) && (lexpType !=  Type.BYTE)){
			throw new SemanticException(
					"Operands to < operator must be Int or Byte",
					node.getLExp().getLine(),
					node.getLExp().getPos());
		}
		this.mCurrentST.setExpType(node, Type.BOOL);
	}

	public void outIntArrayType(IntArrayType node) {

	}
	public void outArrayAssignStatement(ArrayAssignStatement node) {
		STE ste = this.mCurrentST.lookup( node.getIdLit().getLexeme() );
		this.mCurrentST.setNodeSTE( node.getIdLit(), ste);

		if((ste == null) || (!(ste instanceof VarSTE))) {
			throw new SemanticException(
					"Undeclared variable " + node.getIdLit().getLexeme(), 
					node.getIdLit().getLine(), 
					node.getIdLit().getPos());
		}

		VarSTE varEntry = (VarSTE) ste;
		Type arrayType = mCurrentST.getExpType(node.getExp());

		if((varEntry.getType() != Type.COLOR_ARRAY) && (varEntry.getType() != Type.INT_ARRAY)) {
			throw new SemanticException(
					"Array reference to non-array type", 
					node.getIdLit().getLine(), 
					node.getIdLit().getPos());
		}

		if ((mCurrentST.getExpType(node.getIndex()) != Type.INT) && (mCurrentST.getExpType(node.getIndex()) != Type.BYTE)) {
			throw new SemanticException(
					"Index expression type for array reference must be INT or BYTE", 
					node.getIdLit().getLine(), 
					node.getIdLit().getPos());
		}

		if( ((varEntry.getType() == Type.INT_ARRAY) && (arrayType != Type.INT) &&  (arrayType != Type.BYTE)) ||  (varEntry.getType() == Type.COLOR_ARRAY) && (arrayType != Type.COLOR) ) {
			throw new SemanticException(
					"Invalid expression type assigned to variable " + node.getIdLit().getLexeme(), 
					node.getIdLit().getLine(), 
					node.getIdLit().getPos());
		}
	}

	public void outArrayExp(ArrayExp node) {
		if ((this.mCurrentST.getExpType(node.getExp()) != Type.COLOR_ARRAY) && (mCurrentST.getExpType(node.getExp()) != Type.INT_ARRAY))
			throw new SemanticException("Array reference to non-array type", node.getExp().getLine(), node.getExp().getPos());

		if ((this.mCurrentST.getExpType(node.getIndex()) != Type.INT) && (mCurrentST.getExpType(node.getIndex()) != Type.BYTE))
			throw new SemanticException("Index expression type for array reference must be INT or BYTE", node.getIndex().getLine(), node.getIndex().getPos());

		if(mCurrentST.getExpType(node.getExp()) == Type.COLOR_ARRAY)
			mCurrentST.setExpType(node, Type.COLOR);
		else
			mCurrentST.setExpType(node, Type.INT);
	}

	public void outNewArrayExp(NewArrayExp node) {
		if ((mCurrentST.getExpType(node.getExp()) != Type.INT) && (mCurrentST.getExpType(node.getExp()) != Type.BYTE)) {
			throw new SemanticException(
					"Invalid operand type for new array operator", 
					node.getExp().getLine(),
					node.getExp().getPos());
		}
		if(node.getType() instanceof ColorType) {
			mCurrentST.setExpType(node, Type.COLOR_ARRAY);
		}
		else if(node.getType() instanceof IntType) {
			mCurrentST.setExpType(node, Type.INT_ARRAY);
		}
		else {
			throw new SemanticException(
					"Invalid array type for new array operator", 
					node.getExp().getLine(),
					node.getExp().getPos());
		} 
	}


	public void outLengthExp(LengthExp node)
	{
		if ((mCurrentST.getExpType(node.getExp()) != Type.COLOR_ARRAY) && (this.mCurrentST.getExpType(node.getExp()) != Type.INT_ARRAY))
		{
			throw new SemanticException(
					"Operator length called on non-array type",
					node.getExp().getLine(),
					node.getExp().getPos());
		}

		mCurrentST.setExpType(node, Type.INT);
	}

	// Should improve on these:
	private Type typeCheck(Node receiver, String id, List<IExp> args) {
		//check that receiver is of type Class
		Type receiverType = this.mCurrentST.getExpType(receiver);

		if((receiverType == null) || (!receiverType.isClass())) {
			throw new SemanticException("Receiver of method call must be a class type", receiver.getLine(), receiver.getPos());
		}
		//receiverInfo=lookupClass(receiverType.getClassName()) 
		ClassSTE classEntry = this.mCurrentST.lookupClass(receiverType.getName());
		//invocationInfo = receiverInfo.getScope().lookup(funcName); 
		MethodSTE methodEntry = (MethodSTE) classEntry.getScope().lookup(id);
		mCurrentST.setNodeSTE(receiver, methodEntry);
		//if it isn’t there throw exception 
		if(methodEntry == null)
			throw new SemanticException("Method " + id + " does not exist in class type " + classEntry.getName(), receiver.getLine(), receiver.getPos());
		MethodSignature sig = methodEntry.getSignature();
		int i = args.size();
		if(i != sig.numberOfArgs())
			throw new SemanticException("Method " + id + " requires exactly " + sig.numberOfArgs() + " arguments", receiver.getLine(), receiver.getPos());

		IExp[] expList = new IExp[i];
		expList = (IExp[])args.toArray(expList);
		for(i=0; i<expList.length; i++){
			Type expType = this.mCurrentST.getExpType(expList[i]);
			Type sigType = sig.getArg(i);
			if(sigType != expType) 
				throw new SemanticException("Invalid argument type for method " + id, receiver.getLine(), receiver.getPos());
		}

		return sig.getReturnType();
	}

	public void outMeggySetAuxLEDs(MeggySetAuxLEDs node) {
		if(mCurrentST.getExpType(node.getExp()) != Type.INT) {
			throw new SemanticException(
					"Invalid argument type for method MeggySetAuxLEDs", 
					node.getLine(), node.getPos());
		}

	}
}
