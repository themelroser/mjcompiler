package ast_visitors;

import ast.node.*;
import ast.visitor.*;
import exceptions.*;
import symtable.*;

public class RegAllocVisitor extends DepthFirstVisitor {
	
	private final SymTable mCurrentST;
	private int method_offset = 1;
	private int class_offset = 0;
		
	public RegAllocVisitor(SymTable sym) {
		mCurrentST = sym;
	}

	public void outFormal(Formal node) {
		VarSTE varEntry = (VarSTE) mCurrentST.lookup(node.getName());
		varEntry.setLocation("Y", method_offset);
		method_offset += varEntry.getType().getAVRTypeSize();
	}
	
	public void inMethodDecl(MethodDecl node) {
		String name = node.getName();
		mCurrentST.pushScope(name);
		VarSTE varEntry = (VarSTE) mCurrentST.lookup("this");
		varEntry.setLocation("Y", method_offset);
		method_offset += varEntry.getType().getAVRTypeSize();
	}

	public void outMethodDecl(MethodDecl node) {
		mCurrentST.popScope();
		String name = node.getName();
		MethodSTE methodEntry = (MethodSTE) mCurrentST.lookup(name);
		methodEntry.setVarNumBytes(method_offset - 1);
		method_offset = 1;
	}

	public void inTopClassDecl(TopClassDecl node) {
		mCurrentST.pushScope(node.getName());
	}
	
	public void outTopClassDecl(TopClassDecl node) {
		mCurrentST.popScope();
		Type classType = Type.getClassType(node.getName());
		classType.setClassSize(class_offset);
		class_offset = 0;
	}

	public void outVarDecl(VarDecl node) {
		VarSTE varEntry = (VarSTE) mCurrentST.lookup(node.getName());
		if (varEntry.isMember()) {
			varEntry.setLocation("Z", class_offset);
			class_offset += varEntry.getType().getAVRTypeSize();
		} else {
			if (!varEntry.isLocal())
				throw new InternalException("Expecting local var");
			varEntry.setLocation("Y", method_offset);
			method_offset += varEntry.getType().getAVRTypeSize();
		}
	}
}
