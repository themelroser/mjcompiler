package ast_visitors;

import ast.node.*;
import ast.visitor.*;
import exceptions.*
import java.io.*;
import java.util.*;
import symtable.*;

public class AVRregAlloc extends DepthFirstVisitor
{
  private final SymTable mCurrentST;
  private boolean debug = false;

  private int mMethod_offset = 1;

  private int mClass_offset = 0;

  private HashMap<Node, Integer> mNodeToTempID = new HashMap();
  private HashMap<Integer, Integer> mTempIDtoReg = new HashMap();
  private HashSet<Integer> mUsedRegs;
  private HashSet<Integer> mCalleeSavedRegs;
  private int tempCount = 0;

  private boolean mFirstPass = true;

  public AVRregAlloc(SymTable paramSymTable)
  {
    this.mCurrentST = paramSymTable;
  }

  public HashMap<Node, String> getTempMap()
  {
    HashMap localHashMap = new HashMap();
    for (Node localNode : this.mNodeToTempID.keySet()) {
      localHashMap.put(localNode, ((Integer)this.mNodeToTempID.get(localNode)).toString());
    }
    return localHashMap;
  }

}
