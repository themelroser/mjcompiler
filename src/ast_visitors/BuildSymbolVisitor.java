package ast_visitors;

import ast.visitor.DepthFirstVisitor;
import ast.node.*;
import exceptions.*;
import symtable.*;
import java.util.*;
/**
 *
 * Symbol Table Builder Visitor
 *
 *
 *
 */

public class BuildSymbolVisitor extends DepthFirstVisitor {

	private SymTable mCurrentST;
	private boolean mMethodScope = false;
	private ClassSTE mCurrentClass;
	private boolean debug = false;

	public BuildSymbolVisitor()	{
		mCurrentST = new SymTable();
	}

	public SymTable getTable() {
		return mCurrentST;
	}

	public void inTopClassDecl(TopClassDecl node)	{
		//Create ClassSTE
		ClassSTE classEntry;
		if( ! dupeDecl(node.getName()) ) 
			classEntry = new ClassSTE(node.getName(), false);
		else
			classEntry = new ClassSTE(node.getName() + "fake", false);
		
		mCurrentClass = classEntry;

		//Insert and pushScope
		if(debug) System.out.println("Class -- " + classEntry.getName() );
		this.mCurrentST.insertScope(classEntry);
	}
	public void outTopClassDecl(TopClassDecl node)	{
		//popScope
		//if(debug) System.out.println("Class -- Pop");
		mCurrentST.popScope();
	}
	public boolean dupeDecl(String in) {
			
		if ( this.mCurrentST.lookupInnermost( in ) != null) {
			System.err.println("Error: Duplicate Method Declaration For:" + in );
			return true;
		}
		else
			return false;
	}

	public void inMethodDecl(MethodDecl node) {
		mMethodScope = true;

		//Create a list of formals
		Iterator formalIter = node.getFormals().iterator();
		LinkedList<Type> formalList = new LinkedList();
		Formal form = null;

		while(formalIter.hasNext()) {
			form = (Formal) formalIter.next();
			formalList.add( getFormalType( form.getType() ) );
		}
		

		//Create a signature:formalTypes + node type (=return type) 
		Type retType = getFormalType( node.getType() );
		MethodSignature methodSig = new MethodSignature(formalList, retType );
		// create a methodSTE with class&method name, node, signature 
		MethodSTE methodSTE = null;
		if(! dupeDecl(node.getName()) ) {
			methodSTE = new MethodSTE(node.getName(), mCurrentClass.getName(), methodSig, node);
			//System.out.println("    CurrentClass -- " + mCurrentClass.getName() );
			//System.out.println("    CurrentOuter -- " + methodSTE.getOuter() );
		}
		else {
			methodSTE = new MethodSTE(node.getName() + "dupe", mCurrentClass.getName(), methodSig, node);
		}
		mCurrentST.insertScope( methodSTE );
		//if(debug) System.out.println("    CurrentClass -- " + mCurrentClass.getName() );

		//mCurrentST.setExpType(node, retType);
		// insert and pushScope
		if(debug) System.out.println("    Method -- " + node.getName() );
		// create entry for "this"
		VarSTE thisVar = new VarSTE("this", Type.getClassType(this.mCurrentClass.getName()), false, true);
		// Add formals to local scope:
		if(debug) System.out.println("        This -- " + this.mCurrentClass.getName() );
		mCurrentST.setNodeSTE(node, (STE)thisVar);
		mCurrentST.insert( (STE) thisVar );

		formalIter = node.getFormals().iterator();
		VarSTE paramVar = null;

		while (formalIter.hasNext()) {
			form = (Formal) formalIter.next();
			if (this.mCurrentST.lookupInnermost( form.getName() ) == null) {
				paramVar = new VarSTE(form.getName(), getFormalType(form.getType()), false, true);
				if(debug) System.out.println("        Param -- " + form.getName() );
				mCurrentST.setNodeSTE(form, (STE) paramVar); 
				mCurrentST.insert( (STE) paramVar );
			}
		} 
	}

	public void outVarDecl(VarDecl node) {
		if (this.mCurrentST.lookupInnermost( node.getName() ) == null) {
			if(mMethodScope) {
				VarSTE memberVar = new VarSTE(node.getName(), getFormalType(node.getType()), false, false);
				if(debug) System.out.println("    VarDecl -- " + node.getName() );
			 	mCurrentST.insert(memberVar);
			}
			 else {
				VarSTE nonMemberVar = new VarSTE(node.getName(), getFormalType(node.getType()), true, false);
				if(debug) System.out.println("    VarDecl -- " + node.getName() );
			 	mCurrentST.insert(nonMemberVar);
			}
		}
	}
	
	public void outMethodDecl(MethodDecl node) {
		mMethodScope = false;
		//popScope
		//if(debug) System.out.println("    Method -- Pop");
		mCurrentST.popScope();
	}

	public void outIdLiteral(IdLiteral node)
	{
		//TODO:
		//lookup the STE associated with node.getLexeme
		//STE ste = mCurrentST.lookup(node.getLexeme());
		//check that it is in scope, if it is not there, generate a type error
		// get its type IdT, and mCurrentST.setExpType(node, IdT); 
		//Type IdT = node.
	}

	private Type getFormalType(IType formal) {
		if(formal == null)
			throw new InternalException("Null formal");

		Type formalType = Type.INT;

		if(formal instanceof BoolType)
			formalType = Type.BOOL;

		if(formal instanceof IntType)
			formalType = Type.INT;

		if(formal instanceof ByteType)
			formalType = Type.BYTE;

		if(formal instanceof ColorType)
			formalType = Type.COLOR;

		if(formal instanceof ButtonType)
			formalType = Type.BUTTON;

		if(formal instanceof ToneType)
			formalType = Type.TONE;

		if(formal instanceof VoidType)
			formalType = Type.VOID;

		if(formal instanceof ColorArrayType)
			formalType = Type.COLOR_ARRAY;
		
		if(formal instanceof IntArrayType)
			formalType = Type.INT_ARRAY;
		
		if (formal instanceof ClassType) {
			formalType =  Type.getClassType( ( (ClassType) formal).getName() );
		}
		
		return formalType;
	}
	/*
	   private boolean isDefined(Node node) {
	   if (this.mCurrentST.lookupInnermost(node.toStirn) != null) {
	   mError = true;
	   System.err.println("[" + node.getLine() + "," + node.getPos() + "] Redefined symbol ");
	   return true;
	   }
	   return false;
	   }
	   */
}
