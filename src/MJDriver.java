/*
 * MJDriver.java
 *
 * usage: 
 *   java MJ [ --two-pass-interpret | --two-pass-mips ] infile
 *
 * This driver either calls a MiniJava interpreter or generates MIPS
 * code to execute the MiniJava program.
 * The default is the interpreter.
 *
 */
import java.io.FileReader;
import java.io.PrintWriter;

import mjparser.*;
import ast_visitors.*;
//import ast.node.*;
//import ast.visitor.*;
import symtable.*;

public class MJDriver {


	private static void usage() {
		System.err.println("MJ: Specify input file in program arguments");
	}


	public static void main(String args[]) {
		boolean regalloc = false;
		
		if(args.length < 1) {
			usage();
			System.exit(1);
		}
		
		
		if( (args.length > 1) && (args[0].equals("--regalloc"))) {
				regalloc = true;
				System.out.println("Register Allocation Enabled");
		}

		// Filename should be the last command line option
		String filename = args[args.length-1];

		try {
			// Parse file to AST
			Yylex lexer = new Yylex(new FileReader(filename));
			mj parser = new mj(lexer);
			ast.node.Node ast_root = (ast.node.Node)parser.parse().value;
		/*	
			// Print Dot File
			java.io.PrintStream dotStream = new java.io.PrintStream(new java.io.FileOutputStream(filename + ".ast.dot"));
			ast_root.accept(new DotVisitor(new PrintWriter(dotStream)));
			System.out.println("Printing AST to " + filename + ".ast.dot");
		*/
			// Build Symbol Table
			BuildSymbolVisitor buildSym = new BuildSymbolVisitor();
			
			ast_root.accept(buildSym);

			SymTable globalST = buildSym.getTable();
			System.out.println("Symbol Table Built");
			
			// Print Dot File with Map
			java.io.PrintStream dotStream = new java.io.PrintStream(new java.io.FileOutputStream(filename + ".ast.dot"));
			ast_root.accept(new DotVisitorWithMap(new PrintWriter(dotStream), globalST.getMap()));
			System.out.println("Printing AST to " + filename + ".ast.dot");
			
			// Perform Type-Checking 
			ast_root.accept(new CheckTypes(globalST));
			System.out.println("Type-Checking Completed");

			// Allocation
			//ast_root.accept(new RegAllocVisitor(globalST));

			// Generate AVR Code
			java.io.PrintStream avrsout = new java.io.PrintStream(new java.io.FileOutputStream(filename + ".s"));
			ast_root.accept(new AVRgenVisitor(new PrintWriter(avrsout), globalST));
			System.out.println("Printing Atmel assembly to " + filename + ".s");
		} 
		
		catch(exceptions.SemanticException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}  
	}
}
