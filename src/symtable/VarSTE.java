package symtable;

public class VarSTE extends STE {
	
	private Type mType;
	private String mBase;
	private int mOffset;
	private boolean mMemberVar;
	private boolean mParamVar;
	private int mReg = -1;

	public VarSTE(String name, Type type, boolean isMember, boolean isParam) {
		this.mName = name;
		this.mType = type;
		this.mMemberVar = isMember;
		this.mParamVar = isParam;
		this.mBase = "INVALID";
		this.mOffset = 0;
	}

	public void setLocation(String base, int offset) {
		this.mBase = base;
		this.mOffset = offset;
	}

	public void setReg(int reg) {
		mReg = reg;
	}

	public Type getType() {
		return this.mType;
	}

	public String getBase() {
		return mBase;
	}

	public int getOffset() {
		return mOffset;

	}

	public boolean isMember() {
		return this.mMemberVar;
	}

	public boolean isParam() {
		return this.mParamVar;
	}

	public boolean isLocal() {
		return (!this.mParamVar) && (!this.mMemberVar);
	}

	public int getReg() {
		return mReg;
	}
}
