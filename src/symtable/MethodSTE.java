package symtable;

import ast.node.*;
import java.util.*;

public class MethodSTE extends ScopeSTE {

	private MethodSignature mSignature;
	private Node mNode;
	private String mClass;
	private HashSet<Integer> mUsedReg;

	private int mVarNumBytes = 0;


	public MethodSTE(String name, String className, MethodSignature signature, Node node) {

		super(name);
		this.mSignature = signature;
		this.mNode = node;
		mUsedReg = new HashSet();
		this.mClass = className;

	}

	public void setUsedReg(HashSet<Integer> hset) {
		this.mUsedReg = hset;
	}

	public HashSet<Integer> getUsedReg() {
		return mUsedReg;
	}

	public MethodSignature getSignature() {

		return this.mSignature;
	}

	public Node getNode() {
		return this.mNode;
	}

	public int getVarNumBytes() {
		return this.mVarNumBytes;
	}

	public void setVarNumBytes(int paramInt) {
		this.mVarNumBytes = paramInt;
	}
	
	public String getLong() {
		return this.mClass + this.getName();
	}

	public LinkedList<VarSTE> getParamList() {
		LinkedList list = new LinkedList();
		Iterator itr = this.mScope.getOrder().iterator();
		while (itr.hasNext()) {
			STE ste = this.mScope.lookup((String)itr.next());
			if (((ste instanceof VarSTE)) && (((VarSTE) ste).isParam() == true)) {
				list.add((VarSTE)ste);
			}
		  }
		  return list;
	}
}
