package symtable;

import java.util.*;

public class MethodSignature {

	private List<Type> mFormals;
	private Type mReturnType;

	public MethodSignature(List<Type> formals, Type returnType) {

		this.mFormals = formals;
		this.mReturnType = returnType;
	}
	public int numberOfArgs() {

		return mFormals.size();
	}

	public Type getArg(int index) {

		return mFormals.get(index);
	}
	public boolean hasArgs() {

		if(mFormals.size() > 0)
			return true;
		else
			return false;
	}
	public Type getReturnType() {

		return mReturnType;
	}
}
