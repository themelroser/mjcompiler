package symtable;

import exceptions.*;

import java.util.*;

public class Type
{
	public static final Type BOOL = new Type();
	public static final Type INT = new Type();
	public static final Type BYTE = new Type();
	public static final Type COLOR = new Type();
	public static final Type BUTTON = new Type();
	public static final Type TONE = new Type();
	public static final Type VOID = new Type();
	public static final Type COLOR_ARRAY = new Type();
	public static final Type INT_ARRAY = new Type();

	// class type map (key: class name, value: type)
	private static final HashMap<String, Type> classTypes = new HashMap<String, Type>(); 
	
	private final String className;

	private int size = 0;

	private Type()
	{
		className = null;
	}
	
	// method that returns a Type instance for the given class name.
	public static Type getClassType(String name) {
		Type classType = (Type) classTypes.get(name);

		if(classType == null) {
			classType = new Type(name);
			classTypes.put(name, classType);
		}

		return classType;
	}
	 
	// private constructor for a Type instance for a class.
	private Type(String name)
	{
		if(this.className == "") {
			throw new InternalException("unexpected null argument");
		}

		this.className = name;
	
	}

	public String getName() {
		if(className != null) {
			return className;	
		}
		else {
			throw new InternalException("non-reference type");
		}
	}

	public boolean isClass() {
		if(className == null) {
			return false;
		}
		return true;
	}

	public void setClassSize(int size) {
		this.size = size;
	}

	public int getClassSize() {
		return this.size;
	}

	public String toString()
	{
		if(this == INT)
		{
			return "INT";
		}

		if(this == BOOL)
		{
			return "BOOL";
		}

		if(this == BYTE)
		{
			return "BYTE";
		}

		if(this == COLOR)
		{
			return "COLOR";
		}

		if(this == BUTTON)
		{
			return "BUTTON";
		}

		if(this == TONE)
		{
			return "TONE";
		}

		if(this == COLOR_ARRAY) {
			return "[COLOR]";
		}

		if(this == INT_ARRAY) {
			return "[INT]";
		}
		
		return "class_" + this.className + ";";
	}

	public int getAVRTypeSize() {

		if(this == INT) { return 2; }
		if(this == BOOL) { return 1; }
		if(this == BYTE) { return 1; }
		if(this == COLOR) { return 1; }
		if(this == BUTTON) { return 1; }
		if(this == TONE) { return 2; }
		if(this == VOID) { return 0; }

		return 2; // class references are 2 bytes
	}
}
