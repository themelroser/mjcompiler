package symtable;
import java.util.*;
import exceptions.InternalException;

public class Scope {
	private final HashMap<String, STE> mHashMap;
	private final Scope mEnclosing;
	private List<String> mOrder;

	public Scope(Scope enclosingScope)	{
		mHashMap = new HashMap<String, STE>();
		mEnclosing = enclosingScope;
		mOrder = new LinkedList();
	}
	
	public STE lookup(String id) {
		if (mHashMap.containsKey(id)) {
			return (STE) mHashMap.get(id);
		}
		
		if (mEnclosing != null) {
			return mEnclosing.lookup(id);
		}
		
		return null;
	}

	public void insert(STE ste) {
		if ( ( ! (ste instanceof ValueSTE) )  && this.mHashMap.containsKey( ste.getName() ) ) {
			throw new InternalException("Symbol:" + ste.getName() + "has already been defined");
		}
		
		this.mHashMap.put(ste.getName(), ste);
		this.mOrder.add(ste.getName());
	}
	
	public STE lookupInnermost(String id) {
		
		if (this.mHashMap.containsKey(id)) {
			return (STE) mHashMap.get(id);
		}
		
		return null;
	}
	
	public ClassSTE lookupClass(String name) {

		if(mHashMap.containsKey(name) && (mHashMap.get(name) instanceof ClassSTE)) {
			return (ClassSTE) mHashMap.get(name);
		}
		if(mEnclosing != null) {
			return mEnclosing.lookupClass(name);
		}
		return null;
	}

	  public List<String> getOrder() {
		  return this.mOrder;
	  }

	

}
