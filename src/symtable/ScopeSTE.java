package symtable;

public abstract class ScopeSTE extends STE {
	protected Scope mScope;
	public ScopeSTE(String id)	{
		this.mName = id;
	}
	public void setScope(Scope scope) {
		this.mScope = scope;
	}
	public Scope getScope(){
		return this.mScope;
	}
}
